<?php

return [
    'vtex_api' => [
        'key' => 'vtexappkey-taverniti-KPBGGK',
    	'token' => 'FFGVHHIPBDOKFAZZIHKOBJFHGIHIJJSMXMSNNDELFTCANNQVWMIVNKYWZZPYAAGRKCMSHMMFDZJIGFTSSMMXAXZNOWNNOFKENKQLBYNJADSNAQPUGWFOOIQNCZDLZYQX',
    	'accountName' => 'taverniti',
    	'environment' => 'vtexcommercestable',
        'warehouseId' => '1_1',
    	'tradePolicyId' => "1",
        'minQuantity' => 1,
        'url' => [
    		'updateInventoryBySkuAndWarehouse' => 'http://logistics.[environment].com.br/api/logistics/pvt/inventory/skus/[skuid]/warehouses/[warehouseid]?an=[accountname]',
    		'editPrice' => 'https://api.vtex.com/[account]/pricing/prices/[itemid]',
            'skuIdByRefId' => 'http://[accountname].[environment].com.br/api/catalog_system/pvt/sku/stockkeepingunitidbyrefid/[refid]'
    	],
        'connectionTimeout' => 10,
        'timeout' => 30,
        'defaultRange' => [
            'from' => '1900-01-01T03:00:00Z',
            'to' => '4000-01-01T03:00:00Z'
        ],
    ],
    'filePaths' => [
    	'pending' => 'ftp_files/pendientes',
        'processed' => 'ftp_files/procesados',
    ],
    'clearDataAfterProcess' => 1,
];
