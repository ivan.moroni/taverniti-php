<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportPrice extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'ref_id', 'sku_id', 'name', 'price', 'special_price', 'import_date', 'process_date'
	];

	protected $nullable = ['name', 'special_price', 'process_date'];

	protected $table = 'import_price';
	protected $primaryKey = 'import_price_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public $timestamps = false;
}