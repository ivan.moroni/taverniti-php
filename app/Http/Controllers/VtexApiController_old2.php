<?php

namespace App\Http\Controllers;

use Log;

class VtexApiController extends Controller
{
	protected $apiHeaders;
	protected $response;

	public function __construct()
	{
		$this->apiHeaders = [
			'X-VTEX-API-AppKey: '  . config('app.vtex_api.key'),
			'X-VTEX-API-AppToken: ' . config('app.vtex_api.token')
		];

		$this->response = '';
	}

	/**
	 * Gets the sku identifier by reference identifier.
	 *
	 * @param      string  $refId  The reference identifier
	 *
	 * @return     <type>  The sku identifier by reference identifier.
	 */
	public function getSkuIdByRefId($refId)
	{
		if (!in_array('Accept: application/json', $this->apiHeaders)) {
            array_push($this->apiHeaders, 'Accept: application/json');
        }

		$method = 'get';

		$urlParams2Replace = [
			'[accountname]',
			'[environment]',
			'[refid]',
		];

		$urlParams = [
			config('app.vtex_api.accountName'),
			config('app.vtex_api.environment'),
			$refId,
		];

		$url = str_replace($urlParams2Replace, $urlParams, config('app.vtex_api.url.skuIdByRefId'));

		try {
			$this->curlRequest($method, $url, $this->apiHeaders);

			if (is_array($this->response)){
				Log::error('Response Error: ' . json_encode($this->response));
				$this->response = false;
			}

			Log::debug('skuIdByRefId: RefId: ' . $refId . ' Resultado: ' . $this->response);
		} catch (\Exception $e) {
			Log::debug('getSkuIdByRefId: Excepcion capturada: ' . $e->getMessage());
			$this->response = false;
			//throw $e;
		}

		return $this->response;
	}

	/**
	 * Updates Inventory By SKU ID and Warehouse ID
	 *
	 * @param      <type>  $data   The data
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function updateInventoryBySkuAndWarehouse($data)
	{
		array_push($this->apiHeaders, 'Content-Type: application/json; charset=utf-8','Accept: application/json');

		$method = 'put';
		$params = [
			'unlimitedQuantity' => false,
			'dateUtcOnBalanceSystem' => null,
			'quantity' => $data['stock']
		];

		$urlParams2Replace = [
			'[environment]',
			'[skuid]',
			'[warehouseid]',
			'[accountname]'
		];

		$urlParams = [
			config('app.vtex_api.environment'),
			$data['sku_id'],
			config('app.vtex_api.warehouseId'),
			config('app.vtex_api.accountName'),
		];

		$url = str_replace($urlParams2Replace, $urlParams, config('app.vtex_api.url.updateInventoryBySkuAndWarehouse'));

		Log::debug('urlParams2Replace: ' . json_encode($urlParams2Replace) . ' | Params: ' . json_encode($urlParams) . '|original URL: ' . config('app.vtex_api.url.updateInventoryBySkuAndWarehouse'));

		try {
			$this->curlRequest($method, $url, $this->apiHeaders, json_encode($params));
			Log::debug('updateInventoryBySkuAndWarehouse: Resultado: ' . json_encode($this->response));
		} catch (Exception $e) {
			Log::debug('updateInventoryBySkuAndWarehouse: Excepcion capturada: ' . $e->getMessage());
			$this->response = false;
			throw $e;
		}

		return $this->response;
	}

	/**
	 * Updates Price by SKU ID
	 *
	 * @param      <type>  $data   The data
	 *
	 * @return     <type>  ( description_of_the_return_value )
	 */
	public function editPrice($data)
	{
		array_push($this->apiHeaders,'Content-Type: application/json', 'Accept: application/vnd.vtex.pricing.v3+json');

		$urlParams2Replace = [
			'[account]',
			'[itemid]',
		];

		$urlParams = [
			config('app.vtex_api.accountName'),
			$data['sku_id'],
		];

		$url = str_replace($urlParams2Replace, $urlParams, config('app.vtex_api.url.editPrice'));

		$args = [
			'method' => 'put',
			'url' => $url,
			'params' => [
				'listPrice'   => floatval($data['price']),
				'costPrice'   => ($data['special_price'] !== null) ? floatval($data['special_price']) : floatval($data['price']),
				'markup' 	  => 0,
				'fixedPrices' => [
					[
            			'tradePolicyId' => config('app.vtex_api.tradePolicyId'),
            			'value'         => ($data['special_price'] !== null) ? floatval($data['special_price']) : floatval($data['price']),
            			'listPrice'     => floatval($data['price']),
            			'minQuantity'   => config('app.vtex_api.minQuantity'),
            			'dateRange'     =>  [
                			'from' =>  config('app.vtex_api.defaultRange.from'),
               				'to'   =>  config('app.vtex_api.defaultRange.to'),
            			],
        			],
				],
			],
		];

		try {
			$this->savePriceWithRetry($args);

			if (!empty($this->response)) {
				Log::debug('editPrice: Resultado: ' . $this->response);
			} else {
				Log::debug('editPrice: No se obtuvo resultado');
			}
		} catch (Exception $e) {
			Log::error('editPrice: Excepcion capturada: ' . $e->getMessage());
		}
		return $this->response;
	}

	/**
	 * Saves a price with retry.
	 *
	 * @param      <type>  $args   The arguments
	 */
	protected function savePriceWithRetry($args, $price = null)
	{
		try {
            $this->savePrice($args, $price);
        } catch (\Exception $e) {
            $limits = $this->extractPriceLimitsFromResponse();
            if (!empty($limits)) {
                $this->saveBoundedPrice($args, $limits['min'], $limits['max']);
            }
            throw $e;
        }
	}

	/**
	 * Saves a price.
	 *
	 * @param      <type>  $args   The arguments
	 * @param      <type>  $price  The price
	 */
	protected function savePrice($args, $price = null)
	{
		if ($price) {
			$args['params']['listPrice'] = $price;
		}

		try {
			$this->curlRequest($args['method'], $args['url'], $this->apiHeaders, json_encode($args['params']));
			//se pone true como response ya que si esta todo ok, la API no devuelve respuesta.
			$this->response = true;
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Saves a bounded price.
	 *
	 * @param      <type>  $args   The arguments
	 * @param      string  $min    The minimum
	 * @param      string  $max    The maximum
	 */
	protected function saveBoundedPrice($args, $min, $max)
    {
        $targetPrice = floatval($args['price']);

        if ($targetPrice > floatval($max)) {
            Log::debug('Guardando precio maximo permitido: ' . $max. ' - Precio original: '. $price);
            try {
            	$this->savePrice($args, $max);
            	$this->savêPriceWithRetry($args);
            } catch (\Exception $e) {
            	throw $e;
            }
        }

        if ($targetPrice < floatval($min)) {
            Log::debug('Guardando precio minimo permitido: ' . $min. ' - Precio original: '. $price);
            try {
            	$this->savePrice($args, $min);
            	$this->savePriceWithRetry($args);
            } catch (\Exception $e) {
            	throw $e;
            }
        }
    }

	/**
	 * Extracts price limits from Response
	 *
	 * @param      <type>  $response  The response
	 *
	 * @return     array   ( description_of_the_return_value )
	 */
	protected function extractPriceLimitsFromResponse()
    {
        if (!$this->response || !is_string($this->response)) {
            return [];
        }

        $matches = [];
        if (preg_match("/maximum price:\s*([0-9.]*).*minimu[n|m] price:([0-9.]*)/", $this->response, $matches)) {
            if (count($matches) == 3) {
                Log::debug('Extrayendo limites permitidos de precios. Maximo: ' . $matches[1] . ' - Minimo: ' . $matches[2]);
                return [
                    'max' => $matches[1],
                    'min' => $matches[2]
                ];
            }
        }

        return [];
    }

	/**
	 * Prepares and Excecutes CURL request
	 *
	 * @param      string     $method   The method
	 * @param      string     $url      The url
	 * @param      <type>     $headers  The headers
	 * @param      array      $params   The parameters
	 *
	 * @throws     Exception  (description)
	 */
	protected function curlRequest($method, $url, $headers, $params = [])
	{
		try {
			$ch = curl_init($url);
			$responseHeaders = [];

			switch (strtoupper($method)) {
				case 'GET':
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
					if (!empty($params)) {
						$url = $url . '?' . http_build_query($params);
					}
					break;
				case 'POST':
					curl_setopt($ch, CURLOPT_POST, 1);
					if (!empty($params)) {
						curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
					}
					break;
				case 'PUT':
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));
					if (!empty($params)) {
						curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
					}
					break;
				case 'DELETE':
				default:
					throw new \Exception('Metodo no soportado.');
			}

			curl_setopt($ch, CURLOPT_URL, $url);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_ENCODING, "");
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , config('app.vtex_api.connectionTimeout'));
			curl_setopt($ch, CURLOPT_TIMEOUT, config('app.vtex_api.timeout'));

			// obtengo la info de header del response
			curl_setopt($ch, CURLOPT_HEADERFUNCTION,
				function($curl, $header) use (&$responseHeaders) {
				    $len = strlen($header);
				    $header = explode(':', $header, 2);
				    if (count($header) < 2) { // ignore invalid headers
				      return $len;
				    }

				    $name = strtolower(trim($header[0]));
				    if (!array_key_exists($name, $responseHeaders)) {
				      $responseHeaders[$name] = [trim($header[1])];
				    } else {
				      $responseHeaders[$name][] = trim($header[1]);
				    }

				    return $len;
				}
			);

			$response = curl_exec($ch);

			if (curl_error($ch)) {
			    Log::error('CURLRequest ERROR: ' . curl_error($ch));
			    throw new \Exception(curl_error($ch));
			}

			Log::debug('Curl REQUEST: Request:  Method: ' . $method . ' | url:' . $url . ' | headers: ' . json_encode($headers) . ' | params: ' . json_encode($params) . ' || Response: ' . $response);

			//parseo los errores de Vtex que vienen en Header Response
			$errors = $this->parseVtexErrors($responseHeaders);

			if (!empty($errors)) {
			    Log::debug('CURLRequest Vtex ERROR: ' . json_encode($errors));
			    throw new \Exception($errors['message']);
			}

			$this->response = json_decode($response, true);

		} catch (\Exception $e) {
			Log::debug('CURLRequest: Excepcion capturada:' . $e->getMessage());
			throw $e;
		}
	}

	/**
	 * Get Vtex Error code and Message from Header's Response.
	 *
	 * @param      <type>  $headerData  The header data
	 *
	 * @return     array   ( description_of_the_return_value )
	 */
	protected function parseVtexErrors($headerData)
	{
		$vtexError = [];
		if (array_key_exists('x-vtex-error-code', $headerData)) {
			$vtexError = [
				'code'    => $headerData['x-vtex-error-code'][0],
				'message' => $headerData['x-vtex-error-message'][0]
			];
			Log::debug('Vtex ERROR FOUND: ' . json_encode($vtexError));
		}

		return $vtexError;
	}
}
