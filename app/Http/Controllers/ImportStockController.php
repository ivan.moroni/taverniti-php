<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ImportController as ImportController;

use App\ImportStock;
class ImportStockController extends ImportController
{
	public function __construct()
	{
		parent::__construct();
		$this->fileType = 'stock';
		$this->model = new ImportStock();
	}

	/**
     * Loads a data.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function loadData()
    {
        return $this->loadDataFromFile();
    }

    /**
     * Process Update
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function update()
    {
        return $this->processUpdate();
    }

    /**
     * Get data and call external update method
     *
     * @param      <type>  $data   The data
     *
     * @return     array   ( description_of_the_return_value )
     */
    protected function processData($data)
    {
        $response = [
            'status' => -1,
            'msg'    => 'Error Generico'
        ];

        try {
            $updateResult = $this->api->updateInventoryBySkuAndWarehouse($data);

            if ($updateResult) {

                $response['status'] = 0;
                $response['msg'] = 'Ok';

                $item = $this->model->where('sku_id',$data['sku_id'])
                    ->whereNull('process_date')
                    ->first();

                if (!empty($item)) {
                    $item->process_date = date('Y-m-d H:i:s');
                    $item->save();
                }
            }
        } catch (\Exception $e) {
            $response['msg'] = $e->getMessage();
        }

        return $response;
     }

    /**
     * Prepares Stock data to be processed
     *
     * @param      <type>  $fileData  The file data
     *
     * @return     array   ( description_of_the_return_value )
     */
    protected function prepareStockData($fileData)
    {
    	$rowData = [
            'ref_id'      => trim($fileData[0]),
            'name'        => trim($fileData[1]),
            'stock'       => trim($fileData[2]),
            'import_date' => date('Y-m-d H:i:s'),
        ];

        return $rowData;
    }

    /**
     * Validates Stock Values
     *
     * @param      <type>   $stockData  The stock data
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    protected function validateStockValues($stockData)
    {
        $isValid = false;

        if (!empty($stockData['ref_id']) && is_numeric($stockData['stock']) && $stockData['stock'] !== '' && $stockData['stock'] >= 0 ) {
            $isValid = true;
        }

        return $isValid;
    }

}
