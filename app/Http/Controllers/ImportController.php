<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage as Storage;
use App\Http\Controllers\VtexApiController as VtexApiController;
use App\Http\Controllers\VtexApiController as ApiController;
use Log;

//use App\ImportPrice;
use App\ImportStock;

class ImportController extends Controller
{
    protected $fileType;
    protected $model;
    protected $api;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->api = new ApiController();
    }

    /**
     * Loads a data from file.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function loadDataFromFile()
    {
        print('Iniciando importacion de datos para el modulo tipo ' . $this->fileType . '.');

        $pendingPath = config('app.filePaths.pending');
        $pendingFiles = Storage::disk('local')->files($pendingPath);
        $return = false;
        $hasErrors = true;

        if (!empty($pendingFiles)) {
            foreach ($pendingFiles as $pendingFile) {
                $hasErrors = false;
                $csvFile = storage_path('app').'/'.$pendingFile;
                $filePathData = array_reverse(explode('/', $csvFile));

                if (!empty($filePathData)) {
                    $fileName = $filePathData[0];
                    $fileTypeFromFile = $this->getFileType($fileName);

                    if (strtolower($fileTypeFromFile) === $this->fileType) {
                        print('Archivo sin procesar encontrado para el modulo ' . $this->fileType . '. Cargando...');

                        $aFileData = $this->prepareFileData($csvFile);
                        if (!empty($aFileData)) {

                            foreach ($aFileData as $rowId => $row) {
                                //busco en Vtex el skuId por ref_id
                                try {
                                    $skuId = $this->api->getSkuIdByRefId($row['ref_id']);

                                    print('RefID: ' . $row['ref_id'] . ' | skuId: ' . $skuId);

                                    if (!empty($skuId)){
                                        $aFileData[$rowId]['sku_id'] = $skuId;
                                    } else {
                                        //se remueve la fila dado que no se encontro el skuID
                                        print('Descartando ref_id: ' . $row['ref_id'] . '. | SKUId No encontrado.');
                                        unset($aFileData[$rowId]);
                                    }
                                } catch (\Exception $e) {
                                    print('LoadDataFromFile: Excepcion capturada: ' . $e->getMessage());
                                    return $return;
                                }
                            }

                            $dataUploaded = $this->loadFileData($aFileData);

                            if ($dataUploaded) {
                                print('Archivo procesado correctamente. Moviendo a la carpeta Procesados.');

                                $processedPath = config('app.filePaths.processed');
                                $this->moveProcessedFile($fileName, $pendingPath, $processedPath);
                            }
                        }
                    } else {
                        print('Tipo de archivo no soportado: ' . $fileTypeFromFile . '.');
                    }
                }
            }
        } else {
            print('No se encontraron archivos para procesar.');
        }
        print('Importacion de datos para el modulo tipo ' . $this->fileType . ' finalizada.');
        return $return;
    }

    /**
     * Sets the item as processed.
     *
     * @param      <type>  $itemObj   The item object
     * @param      <type>  $itemType  The item type
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    protected function setItemAsProcessed($itemObj, $itemType)
    {
         $model = $this->getModel();
         $itemFromBd = $model->where('sku',$itemObj->sku)->first();
         $return = false;

         if(!empty($itemFromBd)) {
            try {
                $itemFromBd->process_date = date('Y-m-d H:i:s');
                $itemFromBd->save();
                $return = true;
            } catch (Exception $e) {
                print('Excepcion capturada: ' . $e->getMessage());
            }
         }

         return $return;
    }

    /**
     * Process Update
     */
    protected function processUpdate()
    {
        print('Iniciando actualizacion de datos del tipo ' . $this->fileType . '.');

        $initTime = time();
        $pendingPath = config('app.filePaths.pending');
        $hasErrors = false;
        $items = $this->model->where('process_date', null)->get();
        print('Consulta a la base de datos ok ');

        if (!empty($items)) {
            foreach ($items as $item) {

                $processResult = $this->processData($item);
                

                if (!empty($processResult) && $processResult['status'] != 0) {
                    print('Error al momento de procesar resultado: ' . $processResult['msg']);
                    $hasErrors = true;
                }

                print('Item procesado con exito: '. $item['sku_id']);
            }

            if (!$hasErrors) {
                //se eliminan los items ya procesados (si esta configurado de esta manera)
                $this->clearDbData();
            } else {
                 print('hubo errores al procesar los items.');
            }
        }

        $totalTime = time() - $initTime;
        print('Actualizacion de datos del tipo ' . $this->fileType . ' finalizada. Tiempo de ejecucion: ' . $totalTime . ' seg.');
    }

    /**
     * Gets the file type from the filename
     * with <date>_<time>_<stock|price>.csv format.
     *
     * @param      <type>  $fileName  The file name
     *
     * @return     string  The file type.
     */
    protected function getFileType($fileName)
    {
        $fileType = 'undefined';
        $fileData = array_reverse(explode('_', $fileName));

        if (!empty($fileData)) {
            $fileType = explode('.',$fileData[0]);
        }

       return $fileType[0];
    }

    /**
     * Loads a file data.
     *
     * @param      <type>  $fileData  The file data
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    private function loadFileData($fileData)
    {
        $return = false;
        $initTime = time();
        $rowInserted = 0;

        if ($this->model !== null) {
            try {
                foreach ($fileData as $item) {
                    if( !empty($item['sku_id'])) {
                        $this->model::insert($item);
                        $rowInserted++;
                    } else {
                        print('SKU ID No encontrado para ref_id: ' . $item['ref_id'] . '. No se importa a la BD.');
                    }
                }

                $return = true;
            } catch (Exception $e) {
                print('Error: Excepcion: ' . $e->getMessage());
            }
        } else {
            print('Modelo no encontrado.');
        }

        $totalTime = time() - $initTime;
        print('Cantidad de items importados: '. $rowInserted . ' | Tiempo de ejecucion: ' . $totalTime . ' seg.');

        return $return;
    }

    /**
     * Moves processed file from olrPath to newPath folder
     *
     * @param      <type>  $file     The file
     * @param      string  $oldPath  The old path
     * @param      string  $newPath  The new path
     */
    private function moveProcessedFile($file, $oldPath, $newPath)
    {
	$destFileName = date('Ymd_His') . '_' . $file;
        Storage::move($oldPath . '/' . $file , $newPath . '/' . $destFileName);
    }

    /**
     * Prepares files Data
     *
     * @param      <type>  $filePath  The file path
     *
     * @return     array   ( description_of_the_return_value )
     */
    private function prepareFileData($filePath)
    {
        $row = 1;
        $aData = [];
        //1.- Obtengo datos del archivo
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($fileData = fgetcsv($handle, 0, ";")) !== FALSE) {
                if ($row !== 1) {
                    switch ($this->fileType) {
                        case 'stock':
                            $rowData = $this->prepareStockData($fileData);
                            if ($this->validateStockValues($rowData)) {
                                $aData[] = $rowData;
                            }
                            break;
                        case 'price':
                            $rowData = $this->preparePriceData($fileData);
                            if ($this->validatePriceValues($rowData)) {
                                $aData[] = $rowData;
                            }
                            break;
                        default:
                            print('tipo de archivo no soportado.');
                            break;
                    }
                }
                $row++;
            }
            fclose($handle);
        }

        return $aData;
    }

    /**
     * Deletes already processed data if config is set.
     */
    protected function clearDbData()
    {
        if (config('app.clearDataAfterProcess')) {
            print('Iniciando borrado de datos ya procesados en la db.');
            try {
                $deletedRows = $this->model::whereNotNull('process_date')->delete();
                print('Datos procesados borrados exitosamente de la BD: ' . json_encode($deletedRows));
            } catch (\Exception $e) {
                print('No se pudieron borrar los datos procesados de la db. Error: ' . $e->getMessage());
            }
        }
    }
}
