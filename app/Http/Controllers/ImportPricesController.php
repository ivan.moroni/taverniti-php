<?php
namespace App\Http\Controllers;

use App\Http\Controllers\ImportController as ImportController;
use App\ImportPrice;
use Log;

class ImportPricesController extends ImportController
{
	public function __construct()
	{
		parent::__construct();
		$this->fileType = 'price';
		$this->model = new ImportPrice();
	}

	/**
     * Loads data from file.
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function loadData()
    {
        return $this->loadDataFromFile();
    }

    /**
     * update prices
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function update()
    {
        return $this->processUpdate();
    }

    /**
     * Process Data
     *
     * @param      <type>  $data   The data
     *
     * @return     array   ( description_of_the_return_value )
     */
    protected function processData($data)
     {
        $response = [
            'status' => -1,
            'msg' => 'Error Generico'
        ];

        try {
            $updateResult = $this->api->editPrice($data);

            if ($updateResult) {

                $response['status'] = 0;
                $response['msg'] = 'Ok';

                $item = $this->model->where('sku_id',$data['sku_id'])
                    ->whereNull('process_date')
                    ->first();

                if (!empty($item)) {
                    $item->process_date = date('Y-m-d H:i:s');
                    $item->save();
                }
            }
        } catch (\Exception $e) {
            $response['msg'] = $e->getMessage();
        }

        return $response;
     }

    /**
     * Prepares Price Data
     *
     * @param      <type>  $fileData  The file data
     *
     * @return     array   ( description_of_the_return_value )
     */
    protected function preparePriceData($fileData)
    {
    	$rowData = [
            'ref_id'        => trim($fileData[0]),
            'name'          => trim($fileData[1]),
            'price'         => trim($fileData[2]),
            'special_price' => (trim($fileData[3]) !== '') ? trim($fileData[3]) : null,
            'import_date'   => date('Y-m-d H:i:s'),
        ];

        return $rowData;
    }

    /**
     * Validates Proce Values
     *
     * @param      <type>   $priceData  The price data
     *
     * @return     boolean  ( description_of_the_return_value )
     */
    protected function validatePriceValues($priceData)
    {
        $isValid = false;
        //if (!empty($priceData['ref_id']) && is_numeric($priceData['price']) && !empty($priceData['price']) && $priceData['price'] > 0 && (is_numeric($priceData['special_price']) || $priceData['special_price'] === null)) {
        if (!empty($priceData['ref_id']) && is_numeric($priceData['price']) && is_numeric($priceData['price']) && $priceData['price'] >= 0 && ((is_numeric($priceData['special_price']) && $priceData['special_price'] >= 0)|| $priceData['special_price'] === null)) {
            $isValid = true;
        }

        Log::debug('Validado Datos de Precios. Row: '.json_encode($priceData).' | Resultado: ' . $isValid);

        return $isValid;
    }

}
