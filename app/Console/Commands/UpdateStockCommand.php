<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ImportStockController as ImportController;

class UpdateStockCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Stock using Vtex API.';

    protected $controller;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImportController $importController)
    {
        parent::__construct();
        $this->controller = $importController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->controller->update();
    }
}