<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ImportStockController as ImportController;

class LoadStockCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load Stock to DB';

    protected $controller;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImportController $importController)
    {
        parent::__construct();
        $this->controller = $importController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->controller->loadData();
    }
}