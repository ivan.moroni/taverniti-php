<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ImportPricesController as ImportController;
use Log;

class UpdatePricesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prices:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Prices using Vtex API.';

    protected $controller;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImportController $importController)
    {
        parent::__construct();
        $this->controller = $importController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->controller->update();
    }
}